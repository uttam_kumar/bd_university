package com.example.hp.bduniversity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ProgressBar;
import android.widget.TextView;

public class SplashScreen extends AppCompatActivity {
    private ProgressBar progressBar;
    private int progress;
    private TextView textViewProgress;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_splash_screen);

        progressBar=findViewById(R.id.progressBarId);
        textViewProgress=findViewById(R.id.textViewProgressId);

        Thread thread=new Thread(new Runnable() {
            @Override
            public void run() {
                doWork();
                startApp();
            }
        });

        thread.start();

    }

    public void doWork(){

        for(progress=0; progress<=100; progress=progress+20){
            try {
                Thread.sleep(1000);
                progressBar.setProgress(progress);
                textViewProgress.setText(progress+" %");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }


    }


    public void startApp(){
        Intent intent=new Intent(SplashScreen.this,MainActivity.class);
        startActivity(intent);
        finish();
    }
}
