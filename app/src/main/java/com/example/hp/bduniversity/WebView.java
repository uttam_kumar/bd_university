package com.example.hp.bduniversity;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.WebSettings;
import android.webkit.WebViewClient;

public class WebView extends AppCompatActivity {



    private android.webkit.WebView webView;
    private String url;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_view);

        webView= findViewById(R.id.webViewId);

        Intent intent=getIntent();
        url=intent.getStringExtra("key");

        /**
         * for enabelling javaScript, because in default it is desable
         */
        WebSettings webSettings= webView.getSettings();
        webSettings.setJavaScriptEnabled(true);


        /**
         * for desabling new browser access..
         */
        webView.setWebViewClient(new WebViewClient());


        /**
         * link of the website
         */
        webView.loadUrl(url);
    }

    @Override
    public void onBackPressed() {
        if(webView.canGoBack()){
            webView.goBack();
        }else {
            super.onBackPressed();
        }
    }

}
