package com.example.hp.bduniversity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private AutoCompleteTextView autoCompleteTextView;
    private String[] universityName;
    private String[] universityUrl;
    private ListView listView;
    int index;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        autoCompleteTextView=findViewById(R.id.autoCompleteTextViewId);
        listView=findViewById(R.id.listViewId);
        universityName=getResources().getStringArray(R.array.university_name);
        universityUrl=getResources().getStringArray(R.array.university_url);

        ArrayAdapter<String> adapter=new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,universityName);
        autoCompleteTextView.setThreshold(1); //suggestion after writing 1 character
        autoCompleteTextView.setAdapter(adapter);
        listView.setAdapter(adapter);


        autoCompleteTextView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                String value=autoCompleteTextView.getText().toString();

                //Toast.makeText(MainActivity.this,value,Toast.LENGTH_SHORT).show();

                for(int p=0;p<universityName.length;p++){
                    if(universityName[p].equals(value)){
                        index=p;
                    }
                }
                webSite(index);
            }
        });

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                String value=universityName[i];

                for(int p=0;p<universityName.length;p++){
                    if(universityName[p].equals(value)){
                        index=p;
                    }
                }

                webSite(index);
            }
        });


    }

    public void webSite(int l){
        String webUrl= universityUrl[l];
        Log.d("MainActivity", "webSite: "+webUrl);
        //Toast.makeText(MainActivity.this,webUrl+" is Clicked",Toast.LENGTH_SHORT).show();
        Intent intent=new Intent(MainActivity.this,WebView.class);
        intent.putExtra("key",webUrl);
        startActivity(intent);
    }
}
